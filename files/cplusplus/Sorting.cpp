/************************************************************************/
/* Sorting algorithm implementation, including
*		1. SelectionSort(data, length);
*		2. InsertSort(data, length);
*		3. ShellSort(data, length);
*		4. BubbleSort(data, length);
*		5. HeapSort(data, length);
*		6. QuickSort(data, length, start, end);
*		7. MergeSort(data, start, end);
*		8. CountingSort(data, length, kMax);
*		9. RadixSort(data, length, numMaxDigits);
*		10. BucketSort(data, length, kMax);
 * 
 * based on:
 *		[1] http://c.chinaitlab.com/special/cpxsf/
 *		[2] http://blog.csdn.net/touch_2011/article/details/6787364
 * */
/************************************************************************/

#include <stdlib.h>
#include <exception>
#include <stdio.h>
#include <math.h>
#include <iostream>

void Swap(int* num1, int* num2)
{
	int temp = *num1;
	*num1 = *num2;
	*num2 = temp;
}

/************************************************************************/
/* simple selection sort
 * - Idea: select i-th min, then swap with data[i]
 * */
/************************************************************************/
void SelectionSort(int data[], int length)
{
	if (data==NULL || length<=0)
		throw std::exception("Invalid Parameters");

	// n-1 selections, select i-th min each time
	for (int i=0; i<length-1; i++)
	{
		int kMin = i;

		// i-th min is data[kMin]
		for (int j=i+1; j<length; j++)
		{
			if (data[j] < data[kMin])
				kMin = j;
		}

		// swap i-th min with data[i]
		if (i != kMin)
			Swap(&data[kMin], &data[i]);
	}
}

/************************************************************************/
/* straight Insert Sort
 * - Idea: 
 *			1. assume data[1...i-1] has already been ordered, then insert data[i] to data[1...i-1]; 
 *			2. repeat until data[length-1].
 * */
/************************************************************************/
void InsertSort(int data[], int length)
{
	if (data==NULL || length<=0)
		throw std::exception("Invalid Parameters");

	// insert each element into ordered array
	for (int i=1; i<length; i++)
	{
		int temp = data[i];
		int j;

		// insert data[i] into ordered array data[0...i-1]
		for (j=i-1; j>=0 && temp<data[j]; j--)
			data[j+1] = data[j];

		data[j+1] = temp;
	}
}

/************************************************************************/
/* ShellSort
 * - Idea: a modification of InsertSort
 *			1. every time split data[] into several sub-arrays based on given increment value; 
 *			2. for every sub-array, do InsertSort; 
 *			3. then decrease increment value to increase elements of sub-arrays until increment value = 1, 
 *				in which sub-array is the same with data[] and then only simple compare and move will be done.
 * */
/************************************************************************/
void ShellSort(int data[], int length)
{
	if (data==NULL || length<=0)
		throw std::exception("Invalid Parameters");

	// start increment value at length/2, half it every time
	for (int increment=length/2; increment>0; increment/=2)
	{
		for (int i=increment; i<length; i++)
		{
			int temp = data[i];
			int j;

			// do InsertSort for each group elements of current increment value
			for (j=i; j>=increment; j-=increment)
			{
				// put elements>data[j] into back
				if (temp < data[j-increment])
					data[j] = data[j-increment];
				else
					break;
			}


			// put current element at proper position
			data[j] = temp;
		}
	}
}

/************************************************************************/
/* BubbleSort
 * - Idea: 
 *			1. every loop put min to the frontest
 *			2. repeat remaining part (elements-1 after each loop)
 *			3. done when remaining = 1
 * */
/************************************************************************/
void BubbleSort(int data[], int length)
{
	if (data==NULL || length<=0)
		throw std::exception("Invalid Parameters");

	for (int i=0; i<length; i++)
	{
		// record exchange or not for each loop
		bool exchange = false;

		for (int j=i+1; j<length; j++)
		{
			if (data[j] < data[i])
			{
				exchange = true;
				Swap(&data[j], &data[i]);
			}
		}

		// done if no exchange
		if (!exchange)
			break;
	}
}

// HeapAdjust for HeapSort (max heap)
// - data[]: heap array to adjust
// - i: position that need to adjust
// - length: lenght of data[]
void HeapAdjust(int data[], int i, int length)
{
	int temp, nChild;
	for (temp=data[i]; 2*i+1<length; i=nChild)
	{
		// childPostion = parentPosition*2 + 1
		nChild = 2*i + 1;

		// get larger node of 2 child nodes
		if (nChild+1<length && data[nChild+1]>data[nChild])
			nChild++;

		// if larger child node > parent node, then move larger child node up to replace its parent
		if (temp < data[nChild])
			data[i] = data[nChild];
		else // quit loop otherwise
			break;
	}

	// put current element into proper position
	data[i] = temp;
}

/************************************************************************/
/* HeapSort
 * - Idea: 
 *			1. get max of data[]
 *			2. swap max with last element
 *			3. swap will destroy heap property, adjust heap (except last element)
 *			4. repeat 1~3 until done
 * */
/************************************************************************/
void HeapSort(int data[], int length)
{
	if (data==NULL || length<=0)
		throw std::exception("Invalid Parameters");

	// adjust first half array, first will be max when done
	for (int i=length/2-1; i>=0; i--)
		HeapAdjust(data, i, length);

	// adjust start from last one, decrease adjust scope until one 
	for (int i=length-1; i>0; i--)
	{
		// swap first element with current last one, to make sure current last one is max of current array
		Swap(&data[0], &data[i]);

		// decrease adjust scope each time, to make sure first one is max after being adjusted
		HeapAdjust(data, 0, i);
	}
}

// Partition for QuickSort
// - pick pivot element for given sub-array, return pivot position when done
// - all < pivot before pivot, all > pivot before pivot
int Partition(int data[], int length, int start, int end)
{
	// pick first one as the pivot
	int pivot = data[start];

	while (start < end)
	{
		// from back to front, find first element that < pivot 
		while (start<end && data[end]>=pivot)
			end--;

		// swap this element to front part
		Swap(&data[start], &data[end]);

		// from front to back, find first element that > pivot
		while (start<end && data[start]<=pivot)
			start++;

		// swap this element to back part
		Swap(&data[start], &data[end]);
	}

	// return current pivot position
	return start;
}

/************************************************************************/
/* QuickSort
 * - Idea:
 *			1. pick a pivot element and split data[] into two parts, first part all < pivot, second all > pivot
 *			2. repeat for each sub-part
 * */
/************************************************************************/
void QuickSort(int data[], int length, int start, int end)
{
	if(data==NULL || length<=0 || start<0 || end>=length || start>end)
		throw std::exception("Invalid Parameters");

	if (start < end)
	{
		int index = Partition(data, length, start, end);
		if (index > start)
			QuickSort(data, length, start, index-1);
		if (index < end)
			QuickSort(data, length, index+1, end);
	}
}

// Merge for MergeSort
void Merge(int data[], int start, int middle, int end)
{
	int n1 = middle-start+1;
	int n2 = end-middle;
	int * temp1 = new int[n1+1];
	int * temp2 = new int[n2+1];

	// copy first part
	for (int i=0; i<n1; i++)
		temp1[i] = data[start+i];
	
	// copy second part
	for (int j=0; j<n2; j++)
		temp2[j] = data[middle+j+1];

	// set last element of both arrays is very large to avoid check stop conditions  
	temp1[n1] = temp2[n2] = INT_MAX;

	// scan two arrays to put into corresponding positions of data[]
	for (int i=0,j=0,k=start; k<=end;k++)
	{
		if (temp1[i] <= temp2[j])
		{
			data[k] = temp1[i];
			i++;
		}
		else
		{
			data[k] = temp2[j];
			j++;
		}
	}

	delete []temp1;
	delete []temp2;
}

/************************************************************************/
/* MergeSort
 * - Idea: 
 *			1. split data[] into two parts
 *			2. recursively sort for both parts
 *			3. merge both parts together
 * */
/************************************************************************/
void MergeSort(int data[], int start, int end)
{
	if (data==NULL || start<0 || start>end)
		throw std::exception("Invalid Parameters");

	if (start < end)
	{
		int middle = (start+end)/2;

		// sort for first part
		MergeSort(data, start, middle);

		// sort for second part
		MergeSort(data, middle+1, end);

		// merge both parts together
		Merge(data, start, middle, end);
	}
}

/************************************************************************/
/* CountingSort
 * - Idea: for each element x, compute the num of elements < x, then can put x on ordered array based on this num
 *			1. assume all elements of data[] are within [0,nMax], nMax is max
 *			2. use another array count[] record the num of each element in data[]: eg. count[i] stores the num of i in data[]
 *			3. successively change count[] so that count[i] stores the num of elements <=i
 *			4. from back to front, scan data[] and put its elements into another array stub[] based on count[]
 *			5. copy stub[] back to data[]
 * */
/************************************************************************/
void CountingSort(int data[], int length, int nMax)
{
	if (data==NULL || length<=0 || nMax<0)
		throw std::exception("Invalid Parameters");

	int * count = new int[nMax+1];
	int * stub = new int[length];

	// init count[]
	for (int i=0; i<nMax+1; i++)
		count[i] = 0;

	// store num of data[i] in count[]
	for (int i=0; i<length; i++)
		count[data[i]]++;

	// compute the num of elements <=i
	for (int i=1; i<nMax+1; i++)
		count[i] += count[i-1];

	// scan data[] and put its elements on ordered stub[]
	for (int i=length-1; i>=0; i--)
	{
		stub[count[data[i]]-1] = data[i];
		count[data[i]]--;
	}

	for (int i=0; i<length; i++)
		data[i] = stub[i];

	delete []count;
	delete []stub;
}

// CountingSortBasedOnOneDigit for RadixSort
// - will order data[] based on digit-th-bit
void CountingSortBasedOnOneDigit(int data[], int length, int digit)
{
	int * count = new int[10];
	int * stub = new int[length];

	// init count[]
	for (int i=0; i<10; i++)
		count[i] = 0;

	// store the num of elements whose digit-th-bit is i in count[i]
	for (int i=0; i<length; i++)
		count[(data[i]/(int)powf(10, digit-1))%10]++;

	// compute the num of elements whose digit-th-bit <= i and stored in count[i]
	for (int i=1; i<10; i++)
		count[i] += count[i-1];

	// scan data[] and put its elements on ordered stub[]
	for (int i=length-1; i>=0; i--)
	{
		stub[count[(data[i]/(int)powf(10, digit-1))%10]-1] = data[i];
		count[(data[i]/(int)powf(10, digit-1))%10]--;
	}

	for (int i=0; i<length; i++)
		data[i] = stub[i];

	delete []count;
	delete []stub;
}

/************************************************************************/
/* RadixSort (numDigits is the max num digits of data[i], can be larger value than real)
 * - Idea: order data[] based on different digits (from low to high) to decrease size of count[] to 10
 * */
/************************************************************************/
void RadixSort(int data[], int length, int numMaxDigits)
{
	if (data==NULL || length<=0 || numMaxDigits<=0)
		throw std::exception("Invalid Parameters");

	for (int i=1; i<=numMaxDigits; i++)
		CountingSortBasedOnOneDigit(data, length, i);
}

/************************************************************************/
/* BucketSort
 * - Idea: 
 *			1. split data[] into k (=10 here) buckets based on translating function. Note: different functions
 *				can be used to translate the range of elements in array to k buckets. In this implementation, 
 *				based on most significent bit, ie. number = data[i]*10 / (nMax+1); (nMax is the max value of 
 *				data[], can be larger one than real)
 *			2. order each bucket if there are more than one elements
 *			3. list all buckets
 * */
/************************************************************************/
void BucketSort(int data[], int length, int nMax)
{
	if (data==NULL || length<=0 || nMax<0)
		throw std::exception("Invalid Parameters");

	// list node for used in bucket if there are more than one 
	struct Node
	{
		int value;
		Node *next;
	};

	// init 10 buckets
	Node key[10];
	Node *p, *q; //temp variable for node inserting
	for(int i = 0; i < 10; i++)
	{
		key[i].value = 0;
		key[i].next = NULL;
	}

	// split data[] into 10 bucketes
	for(int i = 0; i < length; i++)
	{
		Node *insert = new Node();
		insert->value = data[i];
		insert->next = NULL;
		int number = data[i]*10 / (nMax+1); // transfer to 0~9
		
		if(key[number].next == NULL)
			key[number].next = insert;
		else // link them together if there are more than one elements in current bucket
		{
			p = &key[number];
			q = key[number].next;
			while((q != NULL) && (q->value <= data[i]))
			{
				q = q->next;
				p = p->next;
			}
			insert->next = q;
			p->next = insert;
		}
	}

	// list all buckets together and store back to data[]
	int counter = 0;
	for(int i=0; i<10; i++)
	{
		p = key[i].next;
		if(p == NULL)
			continue;
		while(p != NULL)
		{
			data[counter++] = p->value;
			p = p->next;
		}
	}
 }

int main(int argc, char* argv[])
{
	int data[] = {71,18,151,138,160,63,174,169,79,78};
	int length = sizeof(data) / sizeof(int);
	int start = 0;
	int end = length-1;
	int numMaxDigits = 3; // numMaxDigit of values in data[], can be larger than real
	int kMax = 200; // max value of data[], can be larger than real
	
	try
	{
		//SelectionSort(data, length);
		//InsertSort(data, length);
		//ShellSort(data, length);
		//BubbleSort(data, length);
		//HeapSort(data, length);
		//QuickSort(data, length, start, end);
		//MergeSort(data, start, end);
		//CountingSort(data, length, kMax);
		//RadixSort(data, length, numMaxDigits);
		BucketSort(data, length, kMax);
	}
	catch (std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	
	system("pause");
	return 0;
}