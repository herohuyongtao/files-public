#include "libmyadd2.h"

void main()
{
	std::cout << " function [y,z] = myadd2(a, b) " << std::endl;
	std::cout << " y = a+b; "   << std::endl;
	std::cout << " z = a+2*b; " << '\n' << std::endl;

	/* Initialize the MCR */ 
	if( !mclInitializeApplication(NULL,0) ) 
	{ 
		std::cout << "Could not initialize the application!" << std::endl;
		return; 
	} 

	// Initialize the lib
	if( !libmyadd2Initialize())
	{
		std::cout << "Could not initialize libmyadd2!" << std::endl;
		return; 
	}

	try
	{
		/// 初始化 a
		mwArray a(2, 2,  mxDOUBLE_CLASS);
		a(1,1) = 1;
		a(1,2) = 2;
		a(2,1) = 3;
		a(2,2) = 4;

		std::cout << "a = " << std::endl;
		std::cout << a(1,1) << ","  << a(1,2) << std::endl;
		std::cout << a(2,1) << "," << a(2,2) << '\n' << std::endl;


		/// 初始化 b
		mwArray b(2, 2,  mxDOUBLE_CLASS);
		b(1,1) = 11;
		b(1,2) = 12;
		b(2,1) = 21;
		b(2,2) = 22;

		std::cout << "b = " << std::endl;
		std::cout << b(1,1) << "," << b(1,2) << std::endl;
		std::cout << b(2,1) << "," << b(2,2) <<  '\n' << std::endl;


		/// 定义输出 y z
		mwArray y(2, 2,  mxDOUBLE_CLASS);
		mwArray z(2, 2,  mxDOUBLE_CLASS);


		/// 调用dll
		myadd2(2, y, z, a, b);


		/// 输出结果
		std::cout << "y = " << std::endl;
		std::cout << y(1,1) << "," << y(1,2) << std::endl;
		std::cout << y(2,1) << "," << y(2,2) << '\n' << std::endl;

		std::cout << "z = " << std::endl;
		std::cout << z(1,1) << "," << z(1,2) << std::endl;
		std::cout << z(2,1) << "," << z(2,2) << std::endl;
	}
	catch( const mwException& e)
	{
		std::cerr << e.what() << std::endl;
	}

	// terminate lib
	libmyadd2Terminate();

	// terminate MCR
	mclTerminateApplication();
}