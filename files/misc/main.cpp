/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * Written (W) 2012 Michal Uricar
 * Copyright (C) 2012 Michal Uricar
 */

#include "opencv2/opencv.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>

using namespace std;
using namespace cv;

#include "flandmark_detector.h"

struct FACE
{
	Rect face;

	//Landmarks
	Point2f canthus_rr;
	Point2f canthus_rl;
	Point2f canthus_lr;
	Point2f canthus_ll;
	Point2f mouth_corner_r;
	Point2f mouth_corner_l;
	Point2f nose;
};

float get_overlap_ratio_of_two_rects(Rect r1, Rect r2)
{
	// check they overlap or not
	bool fIntersect = !( r2.x > r1.x+r1.width
		|| r2.x+r2.width < r1.x
		|| r2.y > r1.y+r1.height
		|| r2.y+r2.height < r1.y
		);

	if(fIntersect) 
	{ 
		// get the overlapping rect
		Rect result;

		result.x = max(r1.x, r2.x);
		result.y = max(r1.y, r2.y);
		result.width = min(r1.x+r1.width, r2.x+r2.width)-result.x;
		result.height = min(r1.y+r1.height, r2.y+r2.height)-result.y;

		return result.area()/(float)(r1.area()+r2.area()-result.area());
	} 
	else
		return 0;

	return 0;
}

void detectFaceInImage(IplImage *orig, IplImage* input, CvHaarClassifierCascade* cascade, FLANDMARK_Model *model, int *bbox, double *landmarks)
{
    // Smallest face size.
    CvSize minFeatureSize = cvSize(40, 40);
    int flags =  CV_HAAR_DO_CANNY_PRUNING;
    // How detailed should the search be.
    float search_scale_factor = 1.1f;
    CvMemStorage* storage;
    CvSeq* rects;
    int nFaces;

    storage = cvCreateMemStorage(0);
    cvClearMemStorage(storage);

    // Detect all the faces in the greyscale image.
    rects = cvHaarDetectObjects(input, cascade, storage, search_scale_factor, 2, flags, minFeatureSize);
    nFaces = rects->total;

    double t = (double)cvGetTickCount();
    for (int iface = 0; iface < (rects ? nFaces : 0); ++iface)
    {
        CvRect *r = (CvRect*)cvGetSeqElem(rects, iface);
        
        bbox[0] = r->x;
        bbox[1] = r->y;
        bbox[2] = r->x + r->width;
        bbox[3] = r->y + r->height;
        
        flandmark_detect(input, bbox, model, landmarks);

        // display landmarks
        cvRectangle(orig, cvPoint(bbox[0], bbox[1]), cvPoint(bbox[2], bbox[3]), CV_RGB(255,0,0) );
        cvRectangle(orig, cvPoint(model->bb[0], model->bb[1]), cvPoint(model->bb[2], model->bb[3]), CV_RGB(0,0,255) );
        cvCircle(orig, cvPoint((int)landmarks[0], (int)landmarks[1]), 3, CV_RGB(0, 0,255), CV_FILLED);
        for (int i = 2; i < 2*model->data.options.M; i += 2)
        {
            cvCircle(orig, cvPoint(int(landmarks[i]), int(landmarks[i+1])), 3, CV_RGB(255,0,0), CV_FILLED);

        }
    }
    t = (double)cvGetTickCount() - t;
    int ms = cvRound( t / ((double)cvGetTickFrequency() * 1000.0) );

    if (nFaces > 0)
    {
        printf("Faces detected: %d; Detection of facial landmark on all faces took %d ms\n", nFaces, ms);
    } else {
        printf("NO Face\n");
    }
    
    cvReleaseMemStorage(&storage);
}

vector<FACE> detectFaceInImage_Front_and_Profile(IplImage *orig, IplImage* input, CvHaarClassifierCascade* cascade, CvHaarClassifierCascade* cascade2, FLANDMARK_Model *model, int *bbox, double *landmarks)
{
	vector<FACE> faces;

	// Smallest face size.
	CvSize minFeatureSize = cvSize(80, 80);
	int flags =  CV_HAAR_DO_CANNY_PRUNING;
	// How detailed should the search be.
	float search_scale_factor = 1.1f;
	CvMemStorage* storage;
	CvSeq* rects;
	CvSeq* rects2;
	int nFaces;
	int nFaces2;

	storage = cvCreateMemStorage(0);
	cvClearMemStorage(storage);

	// Detect all the faces in the greyscale image.
	rects = cvHaarDetectObjects(input, cascade, storage, search_scale_factor, 2, flags, minFeatureSize);
	rects2 = cvHaarDetectObjects(input, cascade2, storage, search_scale_factor, 2, flags, minFeatureSize);

	nFaces = rects->total;
	nFaces2 = rects2->total;

	// combile rects and rects2 together, also delete same ones
	for (int iface2 = 0; iface2 < (rects2 ? nFaces2 : 0); ++iface2)
	{
		Rect *r2 = (Rect*)cvGetSeqElem(rects2, iface2);

		for (int iface = 0; iface < (rects ? nFaces : 0); ++iface)
		{
			Rect *r = (Rect*)cvGetSeqElem(rects, iface);

			if (get_overlap_ratio_of_two_rects(*r, *r2) > 0.5)
			{
				nFaces2--;
				cvSeqRemove(rects2, iface2);

				iface2--;
			}
		}
	}

	// start to print out
	double t = (double)cvGetTickCount();
	for (int iface = 0; iface < (rects ? nFaces : 0); ++iface)
	{
		CvRect *r = (CvRect*)cvGetSeqElem(rects, iface);

		bbox[0] = r->x;
		bbox[1] = r->y;
		bbox[2] = r->x + r->width;
		bbox[3] = r->y + r->height;

		// clear landmarks
		for (int i=0; i<2*model->data.options.M; i++)
			landmarks[i] = -1;

		flandmark_detect(input, bbox, model, landmarks);

		// display landmarks
		cvRectangle(orig, cvPoint(bbox[0], bbox[1]), cvPoint(bbox[2], bbox[3]), CV_RGB(255,0,0) );
		cvRectangle(orig, cvPoint(model->bb[0], model->bb[1]), cvPoint(model->bb[2], model->bb[3]), CV_RGB(0,0,255) );
		cvCircle(orig, cvPoint((int)landmarks[0], (int)landmarks[1]), 3, CV_RGB(0, 0,255), CV_FILLED);
		for (int i = 2; i < 2*model->data.options.M; i += 2)
		{
			cvCircle(orig, cvPoint(int(landmarks[i]), int(landmarks[i+1])), 3, CV_RGB(255,0,0), CV_FILLED);
		}

		FACE face;
		face.face = Rect(r->x, r->y, r->width, r->height);
		face.canthus_lr = Point2f(landmarks[2], landmarks[3]);
		face.canthus_rl = Point2f(landmarks[4], landmarks[5]);
		face.mouth_corner_l = Point2f(landmarks[6], landmarks[7]);
		face.mouth_corner_r = Point2f(landmarks[8], landmarks[9]);
		face.canthus_ll = Point2f(landmarks[10], landmarks[11]);
		face.canthus_rr = Point2f(landmarks[12], landmarks[13]);
		face.nose = Point2f(landmarks[14], landmarks[15]);
		faces.push_back(face);
	}
	for (int iface = 0; iface < (rects2 ? nFaces2 : 0); ++iface)
	{
		CvRect *r = (CvRect*)cvGetSeqElem(rects2, iface);

		bbox[0] = r->x;
		bbox[1] = r->y;
		bbox[2] = r->x + r->width;
		bbox[3] = r->y + r->height;

		// clear landmarks
		for (int i=0; i<2*model->data.options.M; i++)
			landmarks[i] = -1;
		flandmark_detect(input, bbox, model, landmarks);

		// display landmarks
		cvRectangle(orig, cvPoint(bbox[0], bbox[1]), cvPoint(bbox[2], bbox[3]), CV_RGB(255,0,0) );
		cvRectangle(orig, cvPoint(model->bb[0], model->bb[1]), cvPoint(model->bb[2], model->bb[3]), CV_RGB(0,0,255) );
		cvCircle(orig, cvPoint((int)landmarks[0], (int)landmarks[1]), 3, CV_RGB(0, 0,255), CV_FILLED);
		for (int i = 2; i < 2*model->data.options.M; i += 2)
		{
			cvCircle(orig, cvPoint(int(landmarks[i]), int(landmarks[i+1])), 3, CV_RGB(255,0,0), CV_FILLED);
		}

		FACE face;
		face.face = Rect(r->x, r->y, r->width, r->height);
		face.canthus_lr = Point2f(landmarks[2], landmarks[3]);
		face.canthus_rl = Point2f(landmarks[4], landmarks[5]);
		face.mouth_corner_l = Point2f(landmarks[6], landmarks[7]);
		face.mouth_corner_r = Point2f(landmarks[8], landmarks[9]);
		face.canthus_ll = Point2f(landmarks[10], landmarks[11]);
		face.canthus_rr = Point2f(landmarks[12], landmarks[13]);
		face.nose = Point2f(landmarks[14], landmarks[15]);
		faces.push_back(face);
	}
	t = (double)cvGetTickCount() - t;
	int ms = cvRound( t / ((double)cvGetTickFrequency() * 1000.0) );

	if (nFaces+nFaces2 > 0)
	{
		printf("Faces detected: %d; Detection of facial landmark on all faces took %d ms\n\n", nFaces+nFaces2, ms);
	} else {
		printf("NO Face\n\n");
	}

	cvReleaseMemStorage(&storage);

	return faces;
}

int main( int argc, char** argv ) 
{
	char flandmark_window[] = "flandmark_simple_example";
	double t;
	int ms;
	int * bbox = (int*)malloc(4*sizeof(int));

	argc = 7;
	argv[1] = "lenna.jpg";
	argv[2] = "85";
	argv[3] = "78";
	argv[4] = "160";
	argv[5] = "170";
	argv[6] = "result.jpg";

	if (argc < 6)
	{
		fprintf(stderr, "Usage: flandmark_1 <path_to_input_image> <face_bbox - 4int> [<path_to_output_image>]\n");
		exit(1);
	}

	//cvNamedWindow(flandmark_window, 0 );

	t = (double)cvGetTickCount();
	FLANDMARK_Model * model = flandmark_init("flandmark_model.dat");
	if (model == 0)
	{
		printf("Structure model wasn't created. Corrupted file flandmark_model.dat?\n");
		exit(1);
	}
	t = (double)cvGetTickCount() - t;
	ms = cvRound( t / ((double)cvGetTickFrequency() * 1000.0) );
	printf("Structure model loaded in %d ms.\n", ms);


	// input image
	IplImage *img = cvLoadImage(argv[1]);
	if (img == NULL)
	{
		//fprintf(stderr, "Wrong path to image. Exiting...\n");
		fprintf(stderr, "Cannot open image %s. Exiting...\n", argv[1]);
		exit(1);
	}

	// convert image to grayscale
	IplImage *img_grayscale = cvCreateImage(cvSize(img->width, img->height), IPL_DEPTH_8U, 1);
	cvCvtColor(img, img_grayscale, CV_BGR2GRAY);

	// face bbox
	bbox[0] = ::atoi(argv[2]);
	bbox[1] = ::atoi(argv[3]);
	bbox[2] = ::atoi(argv[4]);
	bbox[3] = ::atoi(argv[5]);


	// call flandmark_detect
	t = (double)cvGetTickCount();
	double * landmarks = (double*)malloc(2*model->data.options.M*sizeof(double));
	if(flandmark_detect(img_grayscale, bbox, model, landmarks))
	{
		printf("Error during detection.\n");
	}
	t = (double)cvGetTickCount() - t;
	ms = cvRound( t / ((double)cvGetTickFrequency() * 1000.0) );
	printf("Landmarks detected in %d ms.\n", ms);

	cvRectangle(img, cvPoint(bbox[0], bbox[1]), cvPoint(bbox[2], bbox[3]), CV_RGB(255,0,0) );
	cvRectangle(img, cvPoint(model->bb[0], model->bb[1]), cvPoint(model->bb[2], model->bb[3]), CV_RGB(0,0,255) );
	cvCircle(img, cvPoint((int)landmarks[0], (int)landmarks[1]), 3, CV_RGB(0, 0,255), CV_FILLED);
	for (int i = 2; i < 2*model->data.options.M; i += 2)
	{
		cvCircle(img, cvPoint(int(landmarks[i]), int(landmarks[i+1])), 3, CV_RGB(255,0,0), CV_FILLED);

	}
	printf("detection = \t[");
	for (int ii = 0; ii < 2*model->data.options.M; ii+=2)
	{
		printf("%.2f ", landmarks[ii]);
	}
	printf("]\n");
	printf("\t\t[");
	for (int ii = 1; ii < 2*model->data.options.M; ii+=2)
	{
		printf("%.2f ", landmarks[ii]);
	}
	printf("]\n");

	cvShowImage(flandmark_window, img);
	cvWaitKey(0);


	if (argc == 3)
	{
		printf("Saving image to file %s...\n", argv[2]);
		cvSaveImage(argv[2], img);
	}

	// cleanup
	cvDestroyWindow(flandmark_window);
	cvReleaseImage(&img);
	cvReleaseImage(&img_grayscale);
	free(landmarks);
	free(bbox);
	flandmark_free(model);
	 
}
