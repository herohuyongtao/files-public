%% convert all pcms (raw audio file) in selected folder to wav files
%   and save them in the same folder
% 
% !!! NOTES: all pcm files must be recorded with format:
%        (change setting part if needed to suit different cases)
%           a. 'float' format
%           b. fs = 48000
%
%%
function pcm2wav()

% settings, change if needed
format = 'float';
fs = 48000;

% ...
data_pwd = uigetdir;
file_temp = sprintf('%s\\*.pcm', data_pwd);
filearray = dir(file_temp); 
s = max(size(filearray)); 
for i=1: 1: s
    imgname = strcat(data_pwd,'\\',filearray(i).name);

    % read data
    fid = fopen(imgname);
    data = fread(fid, inf, format);
    
    % save it with wanted type to current folder without original extions
    [pathstr, realname, ext] = fileparts(imgname);
    audiowrite([pathstr, realname, '.wav'], data, fs);
end