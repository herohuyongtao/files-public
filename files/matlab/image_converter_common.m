%% convert all images in selected folder from one type to another
%   and save them in the same folder
%   (eg, bmp to ppm ...) by calling image_type_convert('bmp', 'ppm')
%   support types ----------------
%         [bmp, gif, hdf, jpg/jpeg, jp2/jpx, pbm, pcx, pgm, png ...
%             , pnm, ppm, ras, tif/tiff, xwd]
%%
    function image_converter_common(input_type, output_type)

 	data_pwd = uigetdir;
    file_temp = sprintf('%s\\*.%s', data_pwd, input_type);
    
    filearray=dir(file_temp); 
    s=max(size(filearray)); 

    for i=1: 1: s
        imgname=strcat(data_pwd,'\\',filearray(i).name);
        im=imread(imgname) ;

        % save it with wanted type to current folder without input_type extions
        [pathstr,realname,ext] = fileparts(imgname);
        imwrite(im, strcat(pathstr, realname, '.', output_type), output_type);
    end