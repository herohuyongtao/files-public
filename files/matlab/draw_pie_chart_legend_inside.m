clc;clear;

%% data
data_raw = [68 58 64 81 65];
data = [100-data_raw' data_raw'];


%% draw here
figure

subplot(1,5,1)
h = pie(data(1, :)); % 2D pie chart
hp = findobj(h, 'Type', 'patch');
set(hp(1),'FaceColor',[165/255 165/255 165/255]);
set(hp(2),'FaceColor',[90/255 155/255 213/255]);

% put text labels inside the pie chart
hText = findobj(h,'Type','text');
textPositions_cell = get(hText,{'Position'}); % cell array
textPositions = cell2mat(textPositions_cell); % numeric array
textPositions = textPositions * 0.4; % scale position
set(hText,{'Position'},num2cell(textPositions,[3,2])) % set new position

subplot(1,5,2)
h2 = pie(data(2, :)); % 2D pie chart
hp2 = findobj(h2, 'Type', 'patch');
set(hp2(1),'FaceColor',[165/255 165/255 165/255]);
set(hp2(2),'FaceColor',[90/255 155/255 213/255]);

% put text labels inside the pie chart
hText = findobj(h2,'Type','text');
textPositions_cell = get(hText,{'Position'}); % cell array
textPositions = cell2mat(textPositions_cell); % numeric array
textPositions = textPositions * 0.4; % scale position
set(hText,{'Position'},num2cell(textPositions,[3,2])) % set new position

subplot(1,5,3)
h3 = pie(data(3, :)); % 2D pie chart
hp3 = findobj(h3, 'Type', 'patch');
set(hp3(1),'FaceColor',[165/255 165/255 165/255]);
set(hp3(2),'FaceColor',[90/255 155/255 213/255]);

% put text labels inside the pie chart
hText = findobj(h3,'Type','text');
textPositions_cell = get(hText,{'Position'}); % cell array
textPositions = cell2mat(textPositions_cell); % numeric array
textPositions = textPositions * 0.4; % scale position
set(hText,{'Position'},num2cell(textPositions,[3,2])) % set new position

subplot(1,5,4)
h4 = pie(data(4, :)); % 2D pie chart
hp4 = findobj(h4, 'Type', 'patch');
set(hp4(1),'FaceColor',[165/255 165/255 165/255]);
set(hp4(2),'FaceColor',[90/255 155/255 213/255]);

% put text labels inside the pie chart
hText = findobj(h4,'Type','text');
textPositions_cell = get(hText,{'Position'}); % cell array
textPositions = cell2mat(textPositions_cell); % numeric array
textPositions = textPositions * 0.4; % scale position
set(hText,{'Position'},num2cell(textPositions,[3,2])) % set new position

subplot(1,5,5)
h5 = pie(data(5, :)); % 2D pie chart
hp5 = findobj(h5, 'Type', 'patch');
set(hp5(1),'FaceColor',[165/255 165/255 165/255]);
set(hp5(2),'FaceColor',[90/255 155/255 213/255]);

% put text labels inside the pie chart
hText = findobj(h5,'Type','text');
textPositions_cell = get(hText,{'Position'}); % cell array
textPositions = cell2mat(textPositions_cell); % numeric array
textPositions = textPositions * 0.4; % scale position
set(hText,{'Position'},num2cell(textPositions,[3,2])) % set new position


%% lelend
%legend('[Wang et al. 2012]', 'Ours');


%% note for export eps
% 1. font - custom with fixed font size - 12
% 2. move figures text to within the area
% 3. type "export_fig filename -eps" in MATLAB to generate the figure (same as on screen)