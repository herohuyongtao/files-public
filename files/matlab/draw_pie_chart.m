clc;clear;

sex = [152 67];
age = [19 153 36 6 5];
edu = [40 41 68 59 11];
lan = [172 14 43];

%explode = [0 0];
%pie3(sex, explode);
%h = pie3(edu); % 3D pie chart
h = pie(lan); % 2D pie chart
colormap(cool)
%title('Gender')
%title('Age')
%title('Education Background')
%title('Native Language')


%% old style lelend
%legend('Male', 'Female');


%% new style legend
textObjs = findobj(h,'Type','text');
oldStr = get(textObjs,{'String'});
val = get(textObjs,{'Extent'});
oldExt = cat(1,val{:});
%Names = {'Male: '; 'Female: '};
%Names = {'< 20: '; '20 ~ 30: '; '31 ~ 40: '; '41 ~ 50: '; '> 50: '};
%Names = {'< Bachelor: '; 'Bachelor: '; 'Master: '; 'Doctor: '; '> Doctor: '};
Names = {'Chinese: '; 'English: '; 'Other: '};
newStr = strcat(Names,oldStr);
set(textObjs,{'String'},newStr);
val1 = get(textObjs, {'Extent'});
newExt = cat(1, val1{:});
offset = sign(oldExt(:,1)).*(newExt(:,3)-oldExt(:,3))/2;
pos = get(textObjs, {'Position'});
textPos =  cat(1, pos{:});
textPos(:,1) = textPos(:,1)+offset;
set(textObjs,{'Position'},num2cell(textPos,[3,2]));


%% note for export eps
% 1. size - 500x300 points (with option "Expand axes to fill figure" checked)
% 2. font - custom with fixed font size - 20
% 3. type "export_fig filename -eps" in MATLAB to generate the figure (same as on screen)