% convert a video to images
% - through UI to select the video file and the dst folder.

[filename, pathname] = uigetfile({'*.*';'*.avi';'*.mpg';'*.wmv';'*.asf';'*.asx';'*.mj2'}, 'Select a VIDEO file...');
video_file_name = strcat(pathname, filename);
output_images_folder = uigetdir(pwd, 'Select the FOLDER where to save images...') 

mov = VideoReader(video_file_name);
for i=1:1:mov.numberofframes
    b=read(mov,i);
    output_file = sprintf('%s\\%04d.bmp', output_images_folder, i);
    imwrite(b, output_file, 'bmp');
end

msgbox('Done!','Warning...','warn');